package com.abayEthiopianStoreApi.abayethiopianstore.JWT;

import java.util.ArrayList;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.abayEthiopianStoreApi.abayethiopianstore.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CustomUsersDetailsService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;
	
	private com.abayEthiopianStoreApi.abayethiopianstore.entity.User userDetail;

	/**
	 * Load user details by username for authentication and authorization purposes.
	 * @param username The username for which to load the user details.
	 * @return UserDetails object representing the user's authentication and authorization information.
	 * @throws UsernameNotFoundException If the user with the specified username is not found.
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		log.info("Inside loadUserByUsername {}", username);

		// Retrieve user details from the repository based on the email ID
		userDetail = userRepository.findByEmailId(username);

		// Check if user details are found
		if (!Objects.isNull(userDetail)) {
			// Create and return a UserDetails object for Spring Security authentication
			return new org.springframework.security.core.userdetails.User(userDetail.getEmail(), userDetail.getPassword(), new ArrayList<>());
		} else {
			// Throw an exception if the user is not found
			throw new UsernameNotFoundException("User Not Found.");
		}
	}

	/**
	 * Get the user details object for the currently processed user.
	 * @return The User object representing the currently processed user.
	 */
	public com.abayEthiopianStoreApi.abayethiopianstore.entity.User getUserDetail() {
		return userDetail;
	}

}
