package com.abayEthiopianStoreApi.abayethiopianstore.JWT;

import java.io.IOException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class JwtFilter extends OncePerRequestFilter {

	@Autowired
	private JwtUtil jwtUtil;
	
	@Autowired
	private CustomUsersDetailsService customUsersDetailsService;
	
	Claims claims = null;
	private String username = null;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// Extract the "Authorization" header from the HTTP request
		String authorizationHeader = request.getHeader("Authorization");

		// Check if the header is present and starts with "Bearer "
		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			// Extract the token from the header
			String token = authorizationHeader.substring(7); // Extract the token from the header
			try {
				// Extract JWT claims and username from the token
				claims = jwtUtil.extractAllClaims(token);
				username = jwtUtil.extractUsername(token);
				System.out.println("username token value: " + username);
			} catch (Exception e) {
				// Handle any exceptions that might occur during token extraction
				log.error("Error extracting JWT claims: " + e.getMessage());
			}

			// Check if a valid username is extracted and if the user is not already authenticated
			if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
				// Load user details using custom user details service
				UserDetails userDetails = this.customUsersDetailsService.loadUserByUsername(username);

				// Validate the token against the user details
				if (userDetails != null && jwtUtil.validateToken(token, userDetails)) {
					// Create an authentication token and set it in the security context
					UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
							userDetails, null, userDetails.getAuthorities()
					);
					usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
				}
			}
		}
		// Continue the filter chain
		filterChain.doFilter(request, response);
	}

	/**
	 * Check if the authenticated user has the "ROLE_ADMIN" role.
	 * @return True if the user is an admin, otherwise false.
	 */
	public boolean isAdmin() {
		return "ROLE_ADMIN".equalsIgnoreCase((String) claims.get("role"));
	}


	/**
	 * Check if the authenticated user has the "ROLE_USER" role.
	 * @return True if the user is a regular user, otherwise false.
	 */
	public boolean isUser() {
		return "ROLE_USER".equalsIgnoreCase((String) claims.get("role"));
	}

	/**
	 * Get the username of the current authenticated user.
	 * @return The username of the authenticated user.
	 */
	public String getCurrentUser() {
		return username;
	}
	
	

}
