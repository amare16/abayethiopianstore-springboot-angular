package com.abayEthiopianStoreApi.abayethiopianstore.JWT;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;



@Configuration
@EnableWebSecurity
public class SecurityConfig {
	
	@Autowired
	CustomUsersDetailsService customUsersDetailsService;
	
	@Autowired
	JwtFilter jwtFilter;

	/**
	 * Configure the authentication provider to use CustomUsersDetailsService and password encoder.
	 */
    @Bean
    public AuthenticationProvider authenticationProvider() {
    	DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
    	authProvider.setUserDetailsService(customUsersDetailsService);
    	authProvider.setPasswordEncoder(passwordEncoder());
    	return authProvider;
    }

	/**
	 * Provide the AuthenticationManager bean.
	 */
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
      return config.getAuthenticationManager();
    }

	/**
	 * Create a bean for password encoder.
	 */
	@Bean
    public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
    }

	/**
	 * Configure security filters and authorization rules.
	 */
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http
				.cors().and()
				.csrf()
				.disable()
				.authorizeHttpRequests(
						authorizeHttp -> {
							authorizeHttp.requestMatchers("/api/v1/user/**").permitAll();
							authorizeHttp.requestMatchers("/api/v1/dashboard/**").permitAll();
							authorizeHttp.requestMatchers("/api/v1/product/**").permitAll();
							authorizeHttp.requestMatchers("/api/v1/category/**").permitAll();
							authorizeHttp.requestMatchers("/api/v1/bill/**").permitAll();
							authorizeHttp.anyRequest().authenticated();
						}
				)
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.authenticationProvider(authenticationProvider())
				.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);

		return http.build();
		
	}
	
	
}
