package com.abayEthiopianStoreApi.abayethiopianstore.JWT;

import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;


import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;



@Slf4j
@Component
@Service
public class JwtUtil {


	private String secret = "5368566D597133743677397A24432646294A404E635166546A576E5A72347537";

	/**
	 * Extract the username from the given token.
	 * @param token The JWT token from which to extract the username.
	 * @return The extracted username.
	 */
	public String extractUsername(String token) {
		return extractClaims(token, Claims::getSubject);
	}

	/**
	 * Extract the expiration date from the given token.
	 * @param token The JWT token from which to extract the expiration date.
	 * @return The extracted expiration date.
	 */
	public Date extractExpiration(String token) {
		return extractClaims(token,Claims::getExpiration);
	}

	/**
	 * Extract specific claims from the given token using a provided claims resolver function.
	 * @param token The JWT token from which to extract claims.
	 * @param claimsResolver A function that resolves specific claims from a Claims object.
	 * @param <T> The type of the extracted claim.
	 * @return The extracted claim using the provided claims resolver.
	 */
	public <T> T extractClaims(String token, Function<Claims,T> claimsResolver) {
		final Claims claims = extractAllClaims(token);
		return claimsResolver.apply(claims);
	}

	/**
	 * Extract all claims from the given token.
	 * @param token The JWT token from which to extract all claims.
	 * @return The Claims object containing all extracted claims.
	 */
	public Claims extractAllClaims(String token) {
		//return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
		return Jwts
				.parserBuilder()
				.setSigningKey(getSignInKey())
				.build()
				.parseClaimsJws(token)
				.getBody();
	}

	/**
	 * Generate the signing key from the secret.
	 * @return The signing key.
	 */
	private Key getSignInKey() {
		byte[] keyBytes = Decoders.BASE64.decode(secret);
		return Keys.hmacShaKeyFor(keyBytes);
	}

	/**
	 * Check if the given token has expired.
	 * @param token The JWT token to be checked for expiration.
	 * @return True if the token is expired, otherwise false.
	 */
	private Boolean isTokenExpired(String token) {
		// return extractExpiration(token).before(new Date()); // it is my old return
		Date expiration = extractExpiration(token);
		return expiration != null && expiration.before(new Date());
	}

	/**
	 * Generate a JWT token with specified claims and subject.
	 * @param username The subject (user) of the token.
	 * @param role The role to include in the claims.
	 * @return The generated JWT token.
	 */
	public String generateToken(String username, String role) {
		Map<String, Object> claims = new HashMap<>();
		claims.put("role", role);
		return createToken(claims, username);
	}

	/**
	 * Create a JWT token using provided claims and subject, setting issuance and expiration details.
	 * @param claims The claims to include in the token.
	 * @param subject The subject (user) of the token.
	 * @return The created JWT token.
	 */
	private String createToken(Map<String, Object> claims, String subject) {
		Instant expirationTime = Instant.now().plus(30, ChronoUnit.HOURS);
		Date expirationDate = Date.from(expirationTime);
		return Jwts.builder()
				.setClaims(claims)
				.setSubject(subject)
				.setIssuedAt(new Date())
				.setExpiration(expirationDate)
				.signWith(getSignInKey(), SignatureAlgorithm.HS256).compact();
	}

	/**
	 * Validate the JWT token against user details and expiration.
	 * @param token The JWT token to be validated.
	 * @param userDetails The user details against which the token is validated.
	 * @return True if the token is valid, otherwise false.
	 */
	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = extractUsername(token);
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
	}
}
