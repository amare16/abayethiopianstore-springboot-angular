package com.abayEthiopianStoreApi.abayethiopianstore.serviceImpl;


import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.abayEthiopianStoreApi.abayethiopianstore.JWT.CustomUsersDetailsService;
import com.abayEthiopianStoreApi.abayethiopianstore.JWT.JwtFilter;
import com.abayEthiopianStoreApi.abayethiopianstore.JWT.JwtUtil;
import com.abayEthiopianStoreApi.abayethiopianstore.constants.AbayStoreConstants;
import com.abayEthiopianStoreApi.abayethiopianstore.repository.UserRepository;
import com.abayEthiopianStoreApi.abayethiopianstore.entity.User;
import com.abayEthiopianStoreApi.abayethiopianstore.service.UserService;
import com.abayEthiopianStoreApi.abayethiopianstore.utils.AbayStoreUtils;
import com.abayEthiopianStoreApi.abayethiopianstore.utils.EmailUtils;
import com.abayEthiopianStoreApi.abayethiopianstore.wrapper.UserWrapper;
import com.google.common.base.Strings;

import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	CustomUsersDetailsService customUsersDetailsService;
	
	@Autowired
	JwtUtil jwtUtil;
	
	@Autowired
	JwtFilter jwtFilter;
	
	@Autowired
	EmailUtils emailUtils;
	
	private final PasswordEncoder passwordEncoder;
	private final HttpServletResponse response;
	
	
	
	@Override
	public ResponseEntity<String> signUp(Map<String, String> requestMap) {
		// Log the entry of the method with the received requestMap
		// it needs some values like, contactNumber, email, etc
		log.info("Inside signup {}", requestMap);
		
		try {
			// Check if the requestMap contains necessary values for sign-up
			if (validateSignUpMap(requestMap)) {
				// Check if the user with the given email already exists
				User user = userRepository.findByEmailId(requestMap.get("email"));

				if (Objects.isNull(user)) {
					// Save the new user to the repository
					userRepository.save(getUserFromMap(requestMap));
					return AbayStoreUtils.getResponseEntity("User is successfully registered", HttpStatus.OK);
				} else {
					return AbayStoreUtils.getResponseEntity("Email already exists.", HttpStatus.BAD_REQUEST);
				}
			} else {
				// Return a response with an error message for invalid data
				return AbayStoreUtils.getResponseEntity(AbayStoreConstants.INVALID_DATA, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// Handle exceptions, such as database errors or other issues
			e.printStackTrace();
		}
		// Return a response indicating that something went wrong on the server
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		

	}

	// Validate if the requestMap contains all required fields for sign-up
	private boolean validateSignUpMap(Map<String, String> requestMap) {
		// Check if the requestMap contains all necessary keys
		if (requestMap.containsKey("firstName") && requestMap.containsKey("lastName") && requestMap.containsKey("contactNumber") && requestMap.containsKey("email") && requestMap.containsKey("password")) {
			// Return true if all required fields are present
			return true;
		} else {
			// Return false if any required field is missing
			return false;
		}
	}

	// Create a new User object using the data from the requestMap
	private User getUserFromMap(Map<String, String> requestMap) {
		// Create a new User instance
		User user = new User();

		// Set the user properties based on the values in the requestMap
		user.setFirstName(requestMap.get("firstName"));
		user.setLastName(requestMap.get("lastName"));
		user.setContactNumber(requestMap.get("contactNumber"));
		user.setEmail(requestMap.get("email"));
		// Encrypt the password using the passwordEncoder
		user.setPassword(passwordEncoder.encode(requestMap.get("password")));
		// Set initial status to "false"
		user.setStatus("false");
		// Set the role as "ROLE_USER"
		user.setRole("user");

		// Return the created User object
		return user;
	}

	/**
	 * Handle user login authentication.
	 *
	 * @param requestMap A map containing user's email and password.
	 * @return A ResponseEntity with a message indicating the result of login attempt.
	 */
	@Override
	public ResponseEntity<String> login(Map<String, String> requestMap) {

		log.info("Inside login");
		try {
			// Authenticate the user using the provided email and password
			Authentication auth = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(requestMap.get("email"), requestMap.get("password"))
					);
			
			log.info("Auth {}", auth);
			if (auth.isAuthenticated()) {
				// Check if the user's status is approved
				if (customUsersDetailsService.getUserDetail().getStatus().equalsIgnoreCase("true")) {
					//customerUsersDetailsService.getUserDetail().getEmail(), customerUsersDetailsService.getUserDetail().getRole()

					// Generate a JWT token for the user's email and role
					return new ResponseEntity<String>("{\"token\": \"" + jwtUtil.generateToken(customUsersDetailsService.getUserDetail().getEmail(), customUsersDetailsService.getUserDetail().getRole()) + "\"}", HttpStatus.OK);
				} else {
					// Return a response indicating that the user's account needs admin approval
					return new ResponseEntity<String>("{\"message\":\""+"Wait for admin approval!"+"\"}", HttpStatus.BAD_REQUEST);
				}

			}
 		} catch (Exception e) {
			log.error("{}", e);
		}

		// Return a response indicating bad credentials
		return new ResponseEntity<String>("{\"message\":\""+"Bad Credentials!"+"\"}", HttpStatus.BAD_REQUEST);
	}

	/**
	 * Retrieve a list of all users from the repository.
	 *
	 * @return A ResponseEntity containing a list of UserWrapper objects.
	 */
	@Override
	public ResponseEntity<List<UserWrapper>> getAllUsers() {
		try {
			// Check if the user making the request is an admin
			if (jwtFilter.isAdmin()) {
				// Retrieve a list of all users from the repository
				return new ResponseEntity<>(userRepository.getAllListOfUsers(), HttpStatus.OK);
			} else {
				// Return unauthorized status if the user is not an admin
				return new ResponseEntity<>(new ArrayList<>(), HttpStatus.UNAUTHORIZED);
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Return an empty list and internal server error status in case of an exception
		return new ResponseEntity<>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Update the status of a user based on the provided information in the requestMap.
	 *
	 * @param requestMap A Map containing the necessary information for updating user status.
	 * @return A ResponseEntity containing a message indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> updateUser(Map<String, String> requestMap) {
		
		try {
			// Check if the user making the request is an admin
			if (jwtFilter.isAdmin()) {

				// Find the user in the repository by their ID
				Optional<User> optional = userRepository.findById(Integer.parseInt(requestMap.get("id")));
				
				if (!optional.isEmpty()) {
					// Update the status of the user in the repository
					userRepository.updateStatus(requestMap.get("status"), Integer.parseInt(requestMap.get("id")));
					// Send notification emails to all admin users about the user status change
					sendMailToAllAdmin(requestMap.get("status"), optional.get().getEmail(), userRepository.getAllAdmin());
					
					return AbayStoreUtils.getResponseEntity("User Status Updated Successfully!", HttpStatus.OK);
				} else {
					// Return a response indicating that the user ID does not exist
					AbayStoreUtils.getResponseEntity("User id does not exist!", HttpStatus.OK);
				}
			} else {
				// Return a response indicating unauthorized access
				return AbayStoreUtils.getResponseEntity(AbayStoreConstants.INVALID_DATA, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Return a response indicating unauthorized access due to an exception
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.UNAUTHORIZED_ACCESS, HttpStatus.UNAUTHORIZED);
	}

	/**
	 * Send notification emails to all admin users about the user's status change.
	 *
	 * @param status   The updated status of the user.
	 * @param user     The email address of the user.
	 * @param allAdmin A List of email addresses of all admin users.
	 */
	private void sendMailToAllAdmin(String status, String user, List<String> allAdmin) {
		// to stop to send an email twice to the same user
		// If the status is true, send an email indicating that the user's account is approved
		allAdmin.remove(jwtFilter.getCurrentUser());
		
		if (status != null && status.equalsIgnoreCase("true")) {
			// If the status is true, send an email indicating that the user's account is approved
			emailUtils.sendSimpleMessage(jwtFilter.getCurrentUser(),"Account Approved", "USER:- "+user+"\n is approved by \nADMIN:-" + jwtFilter.getCurrentUser(), allAdmin);
		} else {
			// If the status is false, send an email indicating that the user's account is disabled
			emailUtils.sendSimpleMessage(jwtFilter.getCurrentUser(),"Account Disabled", "USER:- "+user+"\n is disabled by \nADMIN:-" + jwtFilter.getCurrentUser(), allAdmin);
		}
	}

	/**
	 * Check the validity of the user's token.
	 * This method is used to verify whether the user's token is valid.
	 *
	 * @return A ResponseEntity indicating the validity of the token.
	 */
	@Override
	public ResponseEntity<String> checkToken() {
		// Respond with a success message and HTTP status OK
		return AbayStoreUtils.getResponseEntity("true", HttpStatus.OK);
	}

	/**
	 * Change the user's password.
	 * This method allows a user to change their password after validating the old password.
	 *
	 * @param requestMap A Map containing the old and new passwords.
	 * @return A ResponseEntity indicating the result of the password change operation.
	 */
	@Override
	public ResponseEntity<String> changePassword(Map<String, String> requestMap) {
		try {
			// Retrieve the current user's information
			User userObj = userRepository.findByEmail(jwtFilter.getCurrentUser());

			// Log the old and new passwords
			log.info("user object value: " + userObj.getPassword());
			log.info("requestMap get oldPassword: " + requestMap.get("oldPassword"));
			log.info("requestMap get newPassword: " + requestMap.get("newPassword"));

			if (userObj != null) {
				// Compare the old password with the hashed password using passwordEncoder.matches()
				if (passwordEncoder.matches(requestMap.get("oldPassword"), userObj.getPassword())) {
					// Encode and update the new password
					userObj.setPassword(passwordEncoder.encode(requestMap.get("newPassword")));
					userRepository.save(userObj);
					return AbayStoreUtils.getResponseEntity("Password Updated Successfully", HttpStatus.OK);
				}
				// Respond with an error message for incorrect old password
				return AbayStoreUtils.getResponseEntity("Incorrect Old Password", HttpStatus.BAD_REQUEST);
			}
			// Respond with an error message for unexpected errors
			return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Respond with an error message for unexpected errors
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Send a password reminder email to the user.
	 * This method allows a user to request a password reminder by sending an email with their password.
	 *
	 * @param requestMap A Map containing the user's email.
	 * @return A ResponseEntity indicating the result of the password reminder email operation.
	 */
	@Override
	public ResponseEntity<String> forgotPassword(Map<String, String> requestMap) {
		try {
			// Find the user by email
			User user = userRepository.findByEmail(requestMap.get("email"));
			
			if (!Objects.isNull(user) && !Strings.isNullOrEmpty(user.getEmail())) {
				// Send a password reminder email to the user's email
				emailUtils.forgotMail(user.getEmail(), "Credentials by Awash Store", user.getPassword());
			}
			// Respond with a success message
			return AbayStoreUtils.getResponseEntity("Check your email for credential ", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Respond with an error message for unexpected errors
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Delete a user by their ID.
	 * This method allows an admin user to delete a user based on their user ID.
	 *
	 * @param userId The ID of the user to be deleted.
	 * @return A ResponseEntity indicating the result of the user deletion operation.
	 */
	@Override
	public ResponseEntity<String> deleteUser(Integer userId) {
		try {
			if (jwtFilter.isAdmin()) {
				Optional optional = userRepository.findById(userId);
				log.info("optional delete user: " + optional);
				if (!optional.isEmpty()) {
					// Delete the user based on the provided user ID
					userRepository.deleteById(userId);
					return AbayStoreUtils.getResponseEntity("User Deleted Successfully!", HttpStatus.OK);
				}
				// Respond with an error message if the user ID doesn't exist
				return AbayStoreUtils.getResponseEntity("Product id doesn't exist", HttpStatus.OK);
			} else {
				// Respond with an unauthorized access error message
				return AbayStoreUtils.getResponseEntity(AbayStoreConstants.UNAUTHORIZED_ACCESS, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Respond with an error message for unexpected errors
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}


}
