package com.abayEthiopianStoreApi.abayethiopianstore.serviceImpl;

import com.abayEthiopianStoreApi.abayethiopianstore.JWT.JwtFilter;
import com.abayEthiopianStoreApi.abayethiopianstore.constants.AbayStoreConstants;
import com.abayEthiopianStoreApi.abayethiopianstore.entity.Bill;
import com.abayEthiopianStoreApi.abayethiopianstore.repository.BillRepository;
import com.abayEthiopianStoreApi.abayethiopianstore.service.BillService;
import com.abayEthiopianStoreApi.abayethiopianstore.utils.AbayStoreUtils;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.io.IOUtils;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
@Service
public class BillServiceImpl implements BillService {

    @Autowired
    JwtFilter jwtFilter;

    @Autowired
    BillRepository billRepository;

    // Method to generate a bill report in PDF format
    @Override
    public ResponseEntity<String> generateBillReport(Map<String, Object> requestMap) {
        log.info("Inside generateReport");
        try {
            String fileName;

            // Validate request data
            if (validateRequestMap(requestMap)) {
                if (requestMap.containsKey("isGenerate") && !(Boolean) requestMap.get("isGenerate")) {
                    fileName = (String) requestMap.get("uuid");
                } else {
                    fileName =AbayStoreUtils.getUUID();
                    System.out.println("file name in generate bill: " + fileName);
                    requestMap.put("uuid", fileName);
                    insertBill(requestMap);
                }

                // Prepare data for the PDF
                String data = "Name: " +requestMap.get("name") + "\n"+"Contact Number: "+requestMap.get("contactNumber")+
                      "\n"+"Email: "+requestMap.get("email")+"\n"+"Payment Method: "+ requestMap.get("paymentMethod");

                // Create and format the PDF
                Document document = new Document();
                PdfWriter.getInstance(document, new FileOutputStream(AbayStoreConstants.STORE_LOCATION+"\\"+fileName+".pdf"));

                document.open();
                setRectangleInPdf(document);

                // Add title and data to the PDF
                Paragraph chunk = new Paragraph("Abay Store Management System", getFont("Header"));
                chunk.setAlignment(Element.ALIGN_CENTER);
                document.add(chunk);

                Paragraph paragraph = new Paragraph(data + "\n \n", getFont("Data"));
                document.add(paragraph);

                // Create and add table to the PDF
                PdfPTable pdfPTable = new PdfPTable(5);
                pdfPTable.setWidthPercentage(100);
                addTableHeader(pdfPTable);

                JSONArray jsonArray = AbayStoreUtils.getJsonArrayFromString((String) requestMap.get("productDetails"));
                for (int i = 0; i < jsonArray.length(); i++) {
                    addRows(pdfPTable, AbayStoreUtils.getMapFromJson(jsonArray.getString(i)));
                }
                document.add(pdfPTable);

                // Add footer to the PDF
                Paragraph footer = new Paragraph("Total : " +requestMap.get("totalAmount")+"\n"
                        +"Thank you for visiting. Please visit again!!", getFont("Data"));
                document.add(footer);
                document.close();
                return new ResponseEntity<>("{\"uuid\":\""+ fileName + "\"}", HttpStatus.OK);

            }
            return AbayStoreUtils.getResponseEntity("Required data is not found", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    // Method to add rows to the PDF table
    private void addRows(PdfPTable pdfPTable, Map<String, Object> data) {
        log.info("Inside addRows");

        // Add cells to the table
        pdfPTable.addCell(String.valueOf(data.get("name")));
        pdfPTable.addCell(String.valueOf(data.get("category")));
        pdfPTable.addCell(String.valueOf(data.get("quantity")));
        pdfPTable.addCell(String.valueOf(data.get("price")));
        pdfPTable.addCell(String.valueOf(data.get("total")));
    }

    // Method to add a header to the PDF table
    private void addTableHeader(PdfPTable pdfPTable) {
        log.info("Inside addTableHeader");

        // Create header cells and add them to the table
        Stream.of("Name", "Category", "Quantity", "Price", "Sub Total")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    header.setBackgroundColor(BaseColor.YELLOW);
                    header.setHorizontalAlignment(Element.ALIGN_CENTER);
                    header.setVerticalAlignment(Element.ALIGN_CENTER);
                    pdfPTable.addCell(header);
                });
    }

    // Method to get font based on type
    private Font getFont(String type) {
        log.info("Inside getFont");

        // Return the appropriate font based on the type
        switch (type) {
            case "Header":
                Font headerFont = FontFactory.getFont(FontFactory.HELVETICA_BOLDOBLIQUE, 18, BaseColor.BLACK);
                headerFont.setStyle(Font.BOLD);
                return headerFont;
            case "Data":
                 Font dataFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK);
                 dataFont.setStyle(Font.BOLD);
                 return dataFont;
            default:
                return new Font();

        }
    }

    // Method to set a rectangle in the PDF document
    private void setRectangleInPdf(Document document) throws DocumentException {
        log.info("Inside setRectangleInPdf");

        // Create a rectangle and add it to the document
        Rectangle rectangle = new Rectangle(577, 825, 18,15);
        rectangle.enableBorderSide(1);
        rectangle.enableBorderSide(2);
        rectangle.enableBorderSide(4);
        rectangle.enableBorderSide(8);
        rectangle.setBorderColor(BaseColor.BLACK);
        rectangle.setBorderWidth(1);
        document.add(rectangle);

    }

    // Method to insert a new bill entry into the database
    private void insertBill(Map<String, Object> requestMap) {
        try {
            Bill bill = new Bill();
            bill.setUuid((String) requestMap.get("uuid"));
            bill.setName((String) requestMap.get("name"));
            bill.setEmail((String) requestMap.get("email"));
            bill.setContactNumber((String) requestMap.get("contactNumber"));
            bill.setPaymentMethod((String) requestMap.get("paymentMethod"));

            // Parse totalAmount from Object to Integer and set in Bill entity
            Integer totalAmount = Integer.parseInt(requestMap.get("totalAmount").toString());
            bill.setTotalAmount(totalAmount);

            bill.setProductDetails((String) requestMap.get("productDetails"));
            bill.setCreatedBy(jwtFilter.getCurrentUser());

            billRepository.save(bill);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Method to validate request data
    private boolean validateRequestMap(Map<String, Object> requestMap) {

        // Validate the presence of required fields in the request map
        return requestMap.containsKey("name") &&
                requestMap.containsKey("contactNumber") &&
                requestMap.containsKey("email") &&
                requestMap.containsKey("paymentMethod") &&
                requestMap.containsKey("productDetails") &&
                requestMap.containsKey("totalAmount");

    }

    // Method to get a list of bills based on user role
    @Override
    public ResponseEntity<List<Bill>> getBills() {
        List<Bill> list = new ArrayList<>();
        if (jwtFilter.isAdmin()) {
            // list all bills on descending order
            list = billRepository.getAllBills();
        } else {
            list = billRepository.getBillByUsername(jwtFilter.getCurrentUser());
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    // Method to retrieve a PDF file based on the provided request map
    @Override
    public ResponseEntity<byte[]> getPdf(Map<String, Object> requestMap) {
        log.info("Inside getPdf: requestMap {}", requestMap);
        try {
            byte[] byteArray = new byte[0];
            if (!requestMap.containsKey("uuid") && validateRequestMap(requestMap))
                return new ResponseEntity<>(byteArray, HttpStatus.BAD_REQUEST);
            String filePath = AbayStoreConstants.STORE_LOCATION + "\\" + (String) requestMap.get("uuid") + ".pdf";
            log.info("filePath in generateBill " + requestMap.get("uuid"));
            if (AbayStoreUtils.isFileExist(filePath)) {
                byteArray = getByteArray(filePath);
                return new ResponseEntity<>(byteArray, HttpStatus.OK);
            } else {
                requestMap.put("isGenerate", false);
                generateBillReport(requestMap);
                byteArray = getByteArray(filePath);
                return new ResponseEntity<>(byteArray, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Error while processing PDF request", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
//        return null;
    }

    /**
     * Reads a file from the given file path and returns its content as a byte array.
     *
     * @param filePath The path of the file to be read.
     * @return A byte array containing the content of the file.
     * @throws Exception If an error occurs while reading the file.
     */
    private byte[] getByteArray(String filePath) throws Exception {
        // Create a File object using the provided file path
        File initialFile = new File(filePath);

        // Create an InputStream to read from the file
        InputStream targetStream = new FileInputStream(initialFile);

        // Convert the InputStream content to a byte array
        byte[] byteArray = IOUtils.toByteArray(targetStream);

        // Close the InputStream to release resources
        targetStream.close();

        // Return the byte array containing the file content
        return byteArray;
    }

    // Method to delete a bill and its associated PDF file from the local PC
    @Override
    public ResponseEntity<String> deleteBillAndPdfFromLocalPc(Integer id) {
        log.info("Inside deleteBill {}", id);
        try {
            Optional<Bill> optional = billRepository.findById(id);
            if (optional.isPresent()) {
                Bill bill = optional.get();
                billRepository.deleteById(id);

                String fileName = bill.getUuid(); // Assuming fileName is the field that stores the name of the PDF file

                String filePath = AbayStoreConstants.STORE_LOCATION + "\\" + fileName + ".pdf";

                if (AbayStoreUtils.isFileExist(filePath)) {
                    File pdfFile = new File(filePath);
                    System.out.println("pdfFile " + pdfFile);
                    boolean isDeleted = pdfFile.delete(); // Delete the PDF file from your computer
                    System.out.println("isDelete " + isDeleted);
                    if (isDeleted) {
                        return AbayStoreUtils.getResponseEntity("Bill and PDF deleted successfully!", HttpStatus.OK);
                    } else {
                        return AbayStoreUtils.getResponseEntity("Failed to delete the PDF from your computer.", HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                } else {
                    return AbayStoreUtils.getResponseEntity("PDF file does not exist on your computer.", HttpStatus.OK);
                }
            } else {
                return AbayStoreUtils.getResponseEntity("Bill ID doesn't exist!", HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
