package com.abayEthiopianStoreApi.abayethiopianstore.serviceImpl;

import com.abayEthiopianStoreApi.abayethiopianstore.repository.BillRepository;
import com.abayEthiopianStoreApi.abayethiopianstore.repository.CategoryRepository;
import com.abayEthiopianStoreApi.abayethiopianstore.repository.ProductRepository;
import com.abayEthiopianStoreApi.abayethiopianstore.repository.UserRepository;
import com.abayEthiopianStoreApi.abayethiopianstore.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class DashboardServiceImpl implements DashboardService {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    BillRepository billRepository;

    @Autowired
    UserRepository userRepository;

    // Get counts of categories, products, bills, and users
    @Override
    public ResponseEntity<Map<String, Object>> getCount() {

        Map<String, Object> map = new HashMap<>();

        // Retrieve and add the count of categories
        map.put("category", categoryRepository.count());

        // Retrieve and add the count of products
        map.put("product", productRepository.count());

        // Retrieve and add the count of bills
        map.put("bill", billRepository.count());

        // Retrieve and add the count of users
        map.put("user", userRepository.count());

        // Return the map containing counts as a response entity
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
