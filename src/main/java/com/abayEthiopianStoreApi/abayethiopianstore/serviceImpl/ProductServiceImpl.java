package com.abayEthiopianStoreApi.abayethiopianstore.serviceImpl;

import com.abayEthiopianStoreApi.abayethiopianstore.JWT.JwtFilter;
import com.abayEthiopianStoreApi.abayethiopianstore.constants.AbayStoreConstants;
import com.abayEthiopianStoreApi.abayethiopianstore.entity.Category;
import com.abayEthiopianStoreApi.abayethiopianstore.entity.Product;
import com.abayEthiopianStoreApi.abayethiopianstore.repository.ProductRepository;
import com.abayEthiopianStoreApi.abayethiopianstore.service.ProductService;
import com.abayEthiopianStoreApi.abayethiopianstore.utils.AbayStoreUtils;
import com.abayEthiopianStoreApi.abayethiopianstore.wrapper.ProductWrapper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    JwtFilter jwtFilter;

    @Override
    public ResponseEntity<String> addNewProduct(Map<String, String> requestMap) {
    	try {
			// Check if the user has admin privileges
            if (jwtFilter.isAdmin()) {
            	log.info("request map: " + requestMap);

				// Validate the product data in the request map
                if (validateProductMap(requestMap, false)) {
                	log.info("request map: " + requestMap);
					// Save the product details in the database
                    productRepository.save(getProductFromMap(requestMap, false));
                    return AbayStoreUtils.getResponseEntity("Product Added Successfully", HttpStatus.OK);
                }
				// Invalid product data in the request
                return AbayStoreUtils.getResponseEntity(AbayStoreConstants.INVALID_DATA, HttpStatus.BAD_REQUEST);
            } else {
				// User is not authorized to add a new product
                return AbayStoreUtils.getResponseEntity(AbayStoreConstants.UNAUTHORIZED_ACCESS, HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		// Something went wrong while processing the request
        return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
    }

	// Validates the product map based on whether an ID is required to be validated
	private boolean validateProductMap(Map<String, String> requestMap, boolean validateId) {
    	if (requestMap.containsKey("name")) {
    		if (requestMap.containsKey("id") && validateId) {
				// Valid product data with ID
    			return true;
    		} else if (!validateId) {
				// Valid product data without ID (for adding new products)
				return true;
			}
    	}
		// Invalid product data
    	return false;
    }

	// Converts the request map into a Product object
	private Product getProductFromMap(Map<String, String> requestMap, boolean isAdd) {
		log.info("get product from map: " + requestMap);
		Category category = new Category();
		// Extract and set the category ID from the map
		category.setId(Integer.parseInt(requestMap.get("categoryId")));
		
		Product product = new Product();
		
		if (isAdd) {
			// If it's an update operation, set the product ID
			product.setId(Integer.parseInt(requestMap.get("id")));
		} else {
			// Set the default status for new products
			product.setStatus("true");
		}
		// Set the category for the product
		product.setCategory(category);
		// Set the name of the product
		product.setName(requestMap.get("name"));
		// Set the description of the product
		product.setDescription(requestMap.get("description"));
		// Set the price of the product
		product.setPrice(Integer.parseInt(requestMap.get("price")));
		log.info("product: " + product);
		return product;
	}


	@Override
	public ResponseEntity<List<ProductWrapper>> getAllProducts() {
		// Retrieve a list of all products from the repository
		try {
			return new ResponseEntity<>(productRepository.getAllProducts(), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Something went wrong while processing the request
		return new ResponseEntity<>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR );
	}

	@Override
	public ResponseEntity<String> updateProduct(Map<String, String> requestMap) {
		try {
			// Check if the user has admin privileges
			if (jwtFilter.isAdmin()) {
				// Validate the updated product data in the request map
				if (validateProductMap(requestMap, true)) {
					// Check if the product with the given ID exists in the database
					Optional<Product> optional = productRepository.findById(Integer.parseInt(requestMap.get("id")));
					if (!optional.isEmpty()) {
						// Update the product details and save in the repository
						Product product = getProductFromMap(requestMap, true);
						product.setStatus(optional.get().getStatus());
						productRepository.save(product);
						
						return AbayStoreUtils.getResponseEntity("Product Updated Successfully!", HttpStatus.OK);
					} else {
						// Product with the given ID doesn't exist
						return AbayStoreUtils.getResponseEntity("Product id doesn't exist", HttpStatus.OK);
					}
				} else {
					// Invalid product data in the request
					return AbayStoreUtils.getResponseEntity(AbayStoreConstants.INVALID_DATA, HttpStatus.BAD_REQUEST);
				}
			} else {
				// User is not authorized to update a product
				return AbayStoreUtils.getResponseEntity(AbayStoreConstants.UNAUTHORIZED_ACCESS, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Something went wrong while processing the request
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<String> deleteProduct(Integer productId) {
		try {
			// Check if the user has admin privileges
			if (jwtFilter.isAdmin()) {
				// Check if the product with the given ID exists in the database
				Optional optional = productRepository.findById(productId);
				if (!optional.isEmpty()) {
					// Delete the product from the repository
					productRepository.deleteById(productId);
					return AbayStoreUtils.getResponseEntity("Product Deleted Successfully!", HttpStatus.OK);
				}
				// Product with the given ID doesn't exist
				return AbayStoreUtils.getResponseEntity("Product id doesn't exist", HttpStatus.OK);
			} else {
				// User is not authorized to delete a product
				return AbayStoreUtils.getResponseEntity(AbayStoreConstants.UNAUTHORIZED_ACCESS, HttpStatus.UNAUTHORIZED);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Something went wrong while processing the request
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	// Update the status of a product based on its ID
	@Override
	public ResponseEntity<String> updateProductStatus(Map<String, String> requestMap) {
		try {
			if (jwtFilter.isAdmin()) {
				Optional optional = productRepository.findById(Integer.parseInt(requestMap.get("id")));
				log.info("request map: " + requestMap);
				if (!optional.isEmpty()) {
					// Update the status of the product using the repository method
					productRepository.updateProductStatus(requestMap.get("status"), Integer.parseInt(requestMap.get("id")));
					return AbayStoreUtils.getResponseEntity("Product Status Updated Successfully!", HttpStatus.OK);
				}
				return AbayStoreUtils.getResponseEntity("Product id doesn't exist", HttpStatus.OK);
			} else {
				return AbayStoreUtils.getResponseEntity(AbayStoreConstants.UNAUTHORIZED_ACCESS, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	// Retrieve a list of products belonging to a category
	@Override
	public ResponseEntity<List<ProductWrapper>> getProductByCategory(Integer id) {
		try {
			Optional optional = productRepository.findById(id);
			if (!optional.isEmpty()) {
				// Retrieve a list of products belonging to the specified category using the repository method
				return new ResponseEntity<>(productRepository.getProductByCategory(id), HttpStatus.OK);
			} else {
				ProductWrapper wrapper = new ProductWrapper("Product not found for ID: " + id);
				return ResponseEntity
						.status(HttpStatus.NOT_FOUND)
						.body(Collections.singletonList(wrapper));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	// Retrieve detailed information about a product by its ID
	@Override
	public ResponseEntity<ProductWrapper> getProductById(Integer id) {
		try {
			Optional optional = productRepository.findById(id);
			if (!optional.isEmpty()) {
				// Retrieve detailed information about a product by its ID using the repository method
				return new ResponseEntity<>(productRepository.getProductById(id), HttpStatus.OK);
			} else {
				ProductWrapper wrapper = new ProductWrapper("Product not found for ID: " + id);
				return ResponseEntity
						.status(HttpStatus.NOT_FOUND)
						.body(wrapper);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ProductWrapper(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
