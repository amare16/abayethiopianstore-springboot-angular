package com.abayEthiopianStoreApi.abayethiopianstore.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.abayEthiopianStoreApi.abayethiopianstore.JWT.JwtFilter;
import com.abayEthiopianStoreApi.abayethiopianstore.constants.AbayStoreConstants;
import com.abayEthiopianStoreApi.abayethiopianstore.repository.CategoryRepository;
import com.abayEthiopianStoreApi.abayethiopianstore.entity.Category;
import com.abayEthiopianStoreApi.abayethiopianstore.service.CategoryService;
import com.abayEthiopianStoreApi.abayethiopianstore.utils.AbayStoreUtils;
import com.google.common.base.Strings;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CategoryRepository categoryRepository;
	
	@Autowired
	JwtFilter jwtFilter;

	// Add a new category to the database
	@Override
	public ResponseEntity<String> addNewCategory(Map<String, String> requestMap) {

		try {
			// Check if the user is an admin
			if (jwtFilter.isAdmin()) {
				// Validate the request map for adding a new category
				if (validateCategoryMap(requestMap, false)) {
					// Save the new category to the repository
					categoryRepository.save(getCategoryFromMap(requestMap, false));
					return AbayStoreUtils.getResponseEntity("Category added successfully!", HttpStatus.OK);
				}
			} else {
				// Return unauthorized access response
				return AbayStoreUtils.getResponseEntity(AbayStoreConstants.UNAUTHORIZED_ACCESS, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Return a response for unexpected errors
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	// Create a Category object from the request map
	private Category getCategoryFromMap(Map<String, String> requestMap, Boolean isAdd) {
		Category category = new Category();
		if (isAdd) {
			category.setId(Integer.parseInt(requestMap.get("id")));
		}
		category.setName(requestMap.get("name"));
		return category;
	}

	// Get a list of all categories
	@Override
	public ResponseEntity<List<Category>> getAllCategories(String filterValue) {
		try {
			if (!Strings.isNullOrEmpty(filterValue) && filterValue.equalsIgnoreCase("true")) {
				log.info("Inside if");
				// Retrieve all categories and return as a response entity
				 return new ResponseEntity<List<Category>>(categoryRepository.getAllCategories(), HttpStatus.OK);
			}
			// Retrieve all categories and return as a response entity
			return new ResponseEntity<>(categoryRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Return an empty list and internal server error status in case of errors
		return new ResponseEntity<>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	// Update an existing category
	@Override
	public ResponseEntity<String> updateCategory(Map<String, String> requestMap) {
		try {
			// Check if the user is an admin
			if (jwtFilter.isAdmin()) {
				log.info("wayyyyyyyyyyyyyy: " + requestMap.get("id"));

				// Validate the request map for updating a category
				if (validateCategoryMap(requestMap, true)) {
					Optional optional = categoryRepository.findById(Integer.parseInt(requestMap.get("id")));
					System.out.println("option category: " + optional);
					if (!optional.isEmpty()) {
						// Update the category in the repository with the new data
						categoryRepository.save(getCategoryFromMap(requestMap, true));
						return AbayStoreUtils.getResponseEntity("Category updated successfully!", HttpStatus.OK);
					} else {
						return AbayStoreUtils.getResponseEntity("Category id doesn't exist!", HttpStatus.OK);
					}
				}
				// Return a response for invalid data
				return AbayStoreUtils.getResponseEntity(AbayStoreConstants.INVALID_DATA, HttpStatus.BAD_REQUEST);
			} else {
				// Return unauthorized access response
				return AbayStoreUtils.getResponseEntity(AbayStoreConstants.UNAUTHORIZED_ACCESS, HttpStatus.UNAUTHORIZED);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		// Return a response for unexpected errors
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	// Validate the request map for category operations
	private boolean validateCategoryMap(Map<String, String> requestMap, boolean validateId) {
		if (requestMap.containsKey("name")) {
			if (requestMap.containsKey("id") && validateId) {
				return true;
			} else if (!validateId) {
				return true;
			}
		}
		return false;
	}

}
