package com.abayEthiopianStoreApi.abayethiopianstore.rest;

import com.abayEthiopianStoreApi.abayethiopianstore.entity.Bill;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * REST API endpoints related to managing bills.
 */
@RequestMapping(path = "api/v1/bill")
public interface BillRest {

    /**
     * Generate a bill report based on the provided request data.
     * @param requestMap The map containing request data.
     * @return A response entity with a message indicating the success of the operation.
     */
    @PostMapping(path = "/generateBillReport")
    ResponseEntity<String> generateBillReport(@RequestBody Map<String, Object> requestMap);

    /**
     * Get a list of all bills.
     * @return A response entity containing a list of Bill objects.
     */
    @GetMapping(path = "/getBills")
    ResponseEntity<List<Bill>> getBills();

    /**
     * Generate a PDF document based on the provided request data.
     * @param requestMap The map containing request data.
     * @return A response entity containing the generated PDF content as bytes.
     */
    @PostMapping(path = "/getPdf")
    ResponseEntity<byte[]> getPdf(@RequestBody Map<String, Object> requestMap);

    /**
     * Delete a bill and its associated PDF document from the local PC based on the provided ID.
     * @param id The ID of the bill to be deleted.
     * @return A response entity with a message indicating the success of the operation.
     */
    @DeleteMapping(path = "/deleteBillAndPdfFromLocalPc/{id}")
    ResponseEntity<String> deleteBillAndPdfFromLocalPc(@PathVariable Integer id);


}
