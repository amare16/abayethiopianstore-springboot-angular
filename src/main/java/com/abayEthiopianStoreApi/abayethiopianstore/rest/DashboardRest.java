package com.abayEthiopianStoreApi.abayethiopianstore.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * REST API endpoints related to dashboard details.
 */
@RequestMapping(path = "api/v1/dashboard")
public interface DashboardRest {

    /**
     * Get dashboard details, such as count information.
     * @return A response entity containing a map of dashboard details.
     */
    @GetMapping(path = "/details")
    //@CrossOrigin(origins = "http://localhost:4200")
    ResponseEntity<Map<String, Object>> getCount();
}
