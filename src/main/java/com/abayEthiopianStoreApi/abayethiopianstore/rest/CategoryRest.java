package com.abayEthiopianStoreApi.abayethiopianstore.rest;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.abayEthiopianStoreApi.abayethiopianstore.entity.Category;

/**
 * REST API endpoints related to managing categories.
 */
@RequestMapping(path = "api/v1/category")
public interface CategoryRest {

	/**
	 * Add a new category using the provided request data.
	 * @param requestMap The map containing request data, including category details.
	 * @return A response entity with a message indicating the success of the operation.
	 */
	@PostMapping(path = "/addCategory")
	ResponseEntity<String> addNewCategory(@RequestBody(required=true) Map<String, String> requestMap);

	/**
	 * Get a list of all categories, optionally filtered by a filter value.
	 * @param filterValue An optional filter value to filter categories.
	 * @return A response entity containing a list of Category objects.
	 */
	@GetMapping(path = "/getAllCategories")
	ResponseEntity<List<Category>> getAllCategories(@RequestParam(required = false) String filterValue);

	/**
	 * Update a category using the provided request data.
	 * @param requestMap The map containing request data, including updated category details.
	 * @return A response entity with a message indicating the success of the operation.
	 */
	@PostMapping(path = "/updateCategory")
	ResponseEntity<String> updateCategory(@RequestBody(required = true) Map<String, String> requestMap);
}
