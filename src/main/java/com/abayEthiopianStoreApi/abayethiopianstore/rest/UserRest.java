package com.abayEthiopianStoreApi.abayethiopianstore.rest;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.abayEthiopianStoreApi.abayethiopianstore.wrapper.UserWrapper;

/**
 * REST API endpoints related to managing users.
 */
@RequestMapping(path = "api/v1/user")
//@CrossOrigin(origins = "http://localhost:4200")
public interface UserRest {

	/**
	 * Sign up a new user using the provided request data.
	 * @param requestMap The map containing request data, including user details.
	 * @return A response entity with a message indicating the success of the operation.
	 */
	@PostMapping(path = "/signup")
	//@CrossOrigin(origins = "http://localhost:4200")
	public ResponseEntity<String>signUp(@RequestBody(required = true) Map<String, String> requestMap);

	/**
	 * Authenticate and log in a user using the provided request data.
	 * @param requestMap The map containing request data, including user credentials.
	 * @return A response entity with a message indicating the success of the operation.
	 */
	@PostMapping(path = "/login")
	//@CrossOrigin(origins = "http://localhost:4200")
	public ResponseEntity<String>login(@RequestBody(required = true) Map<String, String> requestMap);

	/**
	 * Get a list of all users wrapped in UserWrapper.
	 * @return A response entity containing a list of UserWrapper objects.
	 */
	@GetMapping(path = "/getusers")
	//@CrossOrigin(origins = "http://localhost:4200")
	public ResponseEntity<List<UserWrapper>> getAllUsers();

	/**
	 * Update the status of a user using the provided request data.
	 * @param requestMap The map containing request data, including updated user status.
	 * @return A response entity with a message indicating the success of the operation.
	 */
	@PostMapping(path = "/updatestatus")
	public ResponseEntity<String> updateUser(@RequestBody(required = true) Map<String, String> requestMap);

	/**
	 * Check the validity of a token.
	 * @return A response entity with a message indicating the validity of the token.
	 */
	@GetMapping(path = "/checkToken")
	//@CrossOrigin(origins = "http://localhost:4200")
	public ResponseEntity<String> checkToken();

	/**
	 * Change the password of a user using the provided request data.
	 * @param requestMap The map containing request data, including new password.
	 * @return A response entity with a message indicating the success of the operation.
	 */
	@PostMapping(path = "/changePassword")
	ResponseEntity<String> changePassword(@RequestBody Map<String, String> requestMap);

	/**
	 * Initiate the password reset process for a user.
	 * @param requestMap The map containing request data, including user email.
	 * @return A response entity with a message indicating the success of the operation.
	 */
	@PostMapping(path = "/forgotPassword")
	ResponseEntity<String> forgotPassword(@RequestBody Map<String, String> requestMap);

	/**
	 * Delete a user by their ID.
	 * @param userId The ID of the user to be deleted.
	 * @return A response entity with a message indicating the success of the operation.
	 */
	@DeleteMapping(path = "/deleteUser/{userId}")
	ResponseEntity<String> deleteUser(@PathVariable Integer userId);
}
