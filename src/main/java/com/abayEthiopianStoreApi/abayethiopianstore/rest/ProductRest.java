package com.abayEthiopianStoreApi.abayethiopianstore.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.abayEthiopianStoreApi.abayethiopianstore.wrapper.ProductWrapper;

import java.util.List;
import java.util.Map;

/**
 * REST API endpoints related to managing products.
 */
@RequestMapping(path="api/v1/product")
public interface ProductRest {

    /**
     * Add a new product using the provided request data.
     * @param requestMap The map containing request data, including product details.
     * @return A response entity with a message indicating the success of the operation.
     */
    @PostMapping(path = "/addNewProduct")
    ResponseEntity<String> addNewProduct(@RequestBody Map<String, String> requestMap);

    /**
     * Get a list of all products wrapped in ProductWrapper.
     * @return A response entity containing a list of ProductWrapper objects.
     */
    @GetMapping(path = "/getAllProducts")
    ResponseEntity<List<ProductWrapper>> getAllProducts();

    /**
     * Update a product using the provided request data.
     * @param requestMap The map containing request data, including updated product details.
     * @return A response entity with a message indicating the success of the operation.
     */
    @PostMapping(path = "/updateProduct")
    ResponseEntity<String> updateProduct(@RequestBody Map<String, String> requestMap);

    /**
     * Delete a product by its ID.
     * @param productId The ID of the product to be deleted.
     * @return A response entity with a message indicating the success of the operation.
     */
    @DeleteMapping(path = "/deleteProduct/{productId}")
    ResponseEntity<String> deleteProduct(@PathVariable Integer productId);

    /**
     * Update the status of a product using the provided request data.
     * @param requestMap The map containing request data, including updated product status.
     * @return A response entity with a message indicating the success of the operation.
     */
    @PostMapping(path = "/updateProductStatus")
    ResponseEntity<String> updateProductStatus(@RequestBody Map<String, String> requestMap);

    /**
     * Get a list of products belonging to a specific category, wrapped in ProductWrapper.
     * @param id The ID of the category for which to retrieve products.
     * @return A response entity containing a list of ProductWrapper objects.
     */
    @GetMapping(path = "/getProductByCategory/{id}")
    ResponseEntity<List<ProductWrapper>> getProductByCategory(@PathVariable Integer id);

    /**
     * Get a product by its ID, wrapped in ProductWrapper.
     * @param productId The ID of the product to retrieve.
     * @return A response entity containing the requested ProductWrapper object.
     */
    @GetMapping(path = "/getProductById/{id}")
    ResponseEntity<ProductWrapper> getProductById(@PathVariable Integer productId);
}
