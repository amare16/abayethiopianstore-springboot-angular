package com.abayEthiopianStoreApi.abayethiopianstore.service;

import com.abayEthiopianStoreApi.abayethiopianstore.entity.Bill;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

/**
 * Service interface for managing bill-related operations.
 */
public interface BillService {
    /**
     * Generate a bill report based on the provided request data.
     * @param requestMap The map containing request data for generating the report.
     * @return A response entity indicating the success or failure of the operation.
     */
    ResponseEntity<String> generateBillReport(Map<String, Object> requestMap);

    /**
     * Get a list of all bills.
     * @return A response entity containing a list of Bill objects.
     */
    ResponseEntity<List<Bill>> getBills();

    /**
     * Generate a PDF version of a bill based on the provided request data.
     * @param requestMap The map containing request data for generating the PDF.
     * @return A response entity containing the generated PDF data.
     */
    ResponseEntity<byte[]> getPdf(Map<String, Object> requestMap);

    /**
     * Delete a bill and its associated PDF from the local storage based on the provided ID.
     * @param id The ID of the bill to be deleted.
     * @return A response entity indicating the success or failure of the deletion.
     */
    ResponseEntity<String> deleteBillAndPdfFromLocalPc(Integer id);
}
