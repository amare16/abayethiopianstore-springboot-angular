package com.abayEthiopianStoreApi.abayethiopianstore.service;

import org.springframework.http.ResponseEntity;

import com.abayEthiopianStoreApi.abayethiopianstore.wrapper.ProductWrapper;

import java.util.List;
import java.util.Map;

/**
 * Service interface for managing product-related operations.
 */
public interface ProductService {

	/**
	 * Add a new product based on the provided request data.
	 * @param requestMap The map containing request data for adding a new product.
	 * @return A response entity indicating the success or failure of the operation.
	 */
    ResponseEntity<String> addNewProduct(Map<String, String> requestMap);

	/**
	 * Get a list of all products.
	 * @return A response entity containing a list of ProductWrapper objects.
	 */
    ResponseEntity<List<ProductWrapper>> getAllProducts();

	/**
	 * Update an existing product based on the provided request data.
	 * @param requestMap The map containing request data for updating a product.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	ResponseEntity<String> updateProduct(Map<String, String> requestMap);

	/**
	 * Delete a product based on the provided product ID.
	 * @param productId The ID of the product to be deleted.
	 * @return A response entity indicating the success or failure of the deletion.
	 */
	ResponseEntity<String> deleteProduct(Integer productId);

	/**
	 * Update the status of a product based on the provided request data.
	 * @param requestMap The map containing request data for updating the product status.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	ResponseEntity<String> updateProductStatus(Map<String, String> requestMap);

	/**
	 * Get a list of products belonging to a specific category.
	 * @param id The ID of the category.
	 * @return A response entity containing a list of ProductWrapper objects.
	 */
	ResponseEntity<List<ProductWrapper>> getProductByCategory(Integer id);

	/**
	 * Get details of a specific product based on the provided product ID.
	 * @param id The ID of the product.
	 * @return A response entity containing the details of the requested product.
	 */
	ResponseEntity<ProductWrapper> getProductById(Integer id);
}
