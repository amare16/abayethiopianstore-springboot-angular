package com.abayEthiopianStoreApi.abayethiopianstore.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

/**
 * Service interface for managing dashboard-related operations.
 */
public interface DashboardService {

    /**
     * Get counts or statistics related to the dashboard.
     * @return A response entity containing a map of statistics.
     */
    ResponseEntity<Map<String, Object>> getCount();
}
