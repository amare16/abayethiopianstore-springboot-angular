package com.abayEthiopianStoreApi.abayethiopianstore.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.abayEthiopianStoreApi.abayethiopianstore.wrapper.UserWrapper;

/**
 * Service interface for managing user-related operations.
 */
public interface UserService {

	/**
	 * Sign up a new user based on the provided request data.
	 * @param requestMap The map containing request data for user registration.
	 * @return A response entity indicating the success or failure of the registration.
	 */
	ResponseEntity<String> signUp(Map<String, String> requestMap);

	/**
	 * Perform user login based on the provided credentials.
	 * @param requestMap The map containing user login credentials.
	 * @return A response entity indicating the success or failure of the login.
	 */
	ResponseEntity<String> login(Map<String, String> requestMap);

	/**
	 * Get a list of all users.
	 * @return A response entity containing a list of UserWrapper objects.
	 */
	ResponseEntity<List<UserWrapper>> getAllUsers();

	/**
	 * Update an existing user based on the provided request data.
	 * @param requestMap The map containing request data for updating a user.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	ResponseEntity<String> updateUser(Map<String, String> requestMap);

	/**
	 * Check the validity of a user's token.
	 * @return A response entity indicating the token's validity.
	 */
	ResponseEntity<String> checkToken();

	/**
	 * Change the password for a user based on the provided request data.
	 * @param requestMap The map containing request data for changing the password.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	ResponseEntity<String> changePassword(Map<String,String> requestMap);

	/**
	 * Initiate the process of recovering a forgotten password.
	 * @param requestMap The map containing request data for password recovery.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	ResponseEntity<String> forgotPassword(Map<String,String> requestMap);

	/**
	 * Delete a user based on the provided user ID.
	 * @param userId The ID of the user to be deleted.
	 * @return A response entity indicating the success or failure of the deletion.
	 */
	ResponseEntity<String> deleteUser(Integer userId);
}
