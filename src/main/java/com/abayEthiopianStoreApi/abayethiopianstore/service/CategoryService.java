package com.abayEthiopianStoreApi.abayethiopianstore.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.abayEthiopianStoreApi.abayethiopianstore.entity.Category;

/**
 * Service interface for managing category-related operations.
 */
public interface CategoryService {

	/**
	 * Add a new category based on the provided request data.
	 * @param requestMap The map containing request data for adding a new category.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	ResponseEntity<String> addNewCategory(Map<String, String> requestMap);

	/**
	 * Get a list of all categories, optionally filtered by a specific value.
	 * @param filterValue The value to filter categories by (optional).
	 * @return A response entity containing a list of Category objects.
	 */
	ResponseEntity<List<Category>> getAllCategories(String filterValue);

	/**
	 * Update an existing category based on the provided request data.
	 * @param requestMap The map containing request data for updating a category.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	ResponseEntity<String> updateCategory(Map<String, String> requestMap);
}
