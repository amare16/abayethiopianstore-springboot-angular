package com.abayEthiopianStoreApi.abayethiopianstore.repository;

import com.abayEthiopianStoreApi.abayethiopianstore.entity.Product;
import com.abayEthiopianStoreApi.abayethiopianstore.wrapper.ProductWrapper;

import java.util.List;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for managing Product entities in the database.
 */
public interface ProductRepository extends JpaRepository<Product, Integer> {

	/**
	 * Retrieve a list of all products wrapped in ProductWrapper.
	 * @return A list of all products in the database.
	 */
	List<ProductWrapper> getAllProducts();

	/**
	 * Update the status of a product with the given ID.
	 * @param status The new status to set for the product.
	 * @param id The ID of the product to update.
	 * @return The number of updated records.
	 */
	@Modifying
	@Transactional
	Integer updateProductStatus(@Param("status") String status, @Param("id") Integer id);

	/**
	 * Retrieve a list of products belonging to a specific category, wrapped in ProductWrapper.
	 * @param id The ID of the category for which to retrieve products.
	 * @return A list of products in the specified category.
	 */
	List<ProductWrapper> getProductByCategory(@Param("id") Integer id);

	/**
	 * Retrieve a specific product wrapped in ProductWrapper by its ID.
	 * @param id The ID of the product to retrieve.
	 * @return The product with the specified ID.
	 */
	ProductWrapper getProductById(@Param("id") Integer id);
}
