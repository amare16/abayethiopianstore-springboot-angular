package com.abayEthiopianStoreApi.abayethiopianstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;

import com.abayEthiopianStoreApi.abayethiopianstore.entity.User;
import com.abayEthiopianStoreApi.abayethiopianstore.wrapper.UserWrapper;

import jakarta.transaction.Transactional;

/**
 * Repository interface for managing User entities in the database.
 */
public interface UserRepository extends JpaRepository<User, Integer> {

	/**
	 * Find a user by their email ID.
	 * @param email The email ID of the user to find.
	 * @return The user with the specified email ID.
	 */
	User findByEmailId(@Param("email") String email);

	/**
	 * Retrieve a list of all users wrapped in UserWrapper.
	 * @return A list of all users in the database.
	 */
	List<UserWrapper> getAllListOfUsers();

	/**
	 * Retrieve a list of email addresses of all users with admin role.
	 * @return A list of email addresses of admin users.
	 */
	List<String> getAllAdmin();

	/**
	 * Update the status of a user with the given ID.
	 * @param status The new status to set for the user.
	 * @param id The ID of the user to update.
	 * @return The number of updated records.
	 */
	@Transactional
	@Modifying
	Integer updateStatus(@Param("status") String status,@Param("id") Integer id);

	/**
	 * Find a user by their email.
	 * @param email The email of the user to find.
	 * @return The user with the specified email.
	 */
	User findByEmail(String email);
}
