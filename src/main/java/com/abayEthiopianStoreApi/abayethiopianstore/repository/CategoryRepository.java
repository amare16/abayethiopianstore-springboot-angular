package com.abayEthiopianStoreApi.abayethiopianstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abayEthiopianStoreApi.abayethiopianstore.entity.Category;

/**
 * Repository interface for managing Category entities in the database.
 */
public interface CategoryRepository extends JpaRepository<Category, Integer> {

	/**
	 * Retrieve a list of all categories.
	 * @return A list of all categories in the database.
	 */
	List<Category> getAllCategories();
}
