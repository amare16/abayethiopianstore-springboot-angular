package com.abayEthiopianStoreApi.abayethiopianstore.repository;

import com.abayEthiopianStoreApi.abayethiopianstore.entity.Bill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Repository interface for managing Bill entities in the database.
 */
public interface BillRepository extends JpaRepository<Bill, Integer> {

    /**
     * Retrieve a list of all bills.
     * @return A list of all bills in the database.
     */
    List<Bill> getAllBills();

    /**
     * Retrieve a list of bills associated with a specific username.
     * @param username The username for which to retrieve bills.
     * @return A list of bills associated with the given username.
     */
    List<Bill> getBillByUsername(@Param("username") String username);

}
