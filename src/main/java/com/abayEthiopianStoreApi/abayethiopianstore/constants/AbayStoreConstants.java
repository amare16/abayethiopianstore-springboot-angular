package com.abayEthiopianStoreApi.abayethiopianstore.constants;

public class AbayStoreConstants {

	public static final String SOMETHING_WENT_WRONG = "Something went wrong.";
	
	public static final String INVALID_DATA = "Invalid Data";
	
	public static final String UNAUTHORIZED_ACCESS = "Unauthorized access";

	public static final String STORE_LOCATION = "D:\\eclipse-workspace\\abayethiopianstore\\billStore";
}
