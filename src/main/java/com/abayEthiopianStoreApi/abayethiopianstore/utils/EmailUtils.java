package com.abayEthiopianStoreApi.abayethiopianstore.utils;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

@Service
public class EmailUtils {

	@Autowired
	private JavaMailSender emailSender;

	/**
	 * Send a simple text email message.
	 *
	 * @param to      The recipient's email address.
	 * @param subject The subject of the email.
	 * @param text    The content of the email.
	 * @param list    A list of email addresses to include in the CC field.
	 */
//	@Async
	public void sendSimpleMessage(String to, String subject, String text, List<String> list) {

		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("amarenighatu16@gmail.com");
		message.setTo(to);
		message.setSubject(subject);
		message.setText(text);

		if (list != null && list.size() > 0) {
			message.setCc(getCcArray(list));
		}
		emailSender.send(message);

		System.out.println("Mail sent successfully...");

	}

	/**
	 * Convert a list of email addresses to an array for CC.
	 *
	 * @param ccList A list of email addresses.
	 * @return An array of email addresses for CC.
	 */
	private String[] getCcArray(List<String> ccList) {
		String[] cc= new String[ccList.size()];

		for (int i = 0; i < ccList.size(); i++) {
			cc[i] = ccList.get(i);
		}
		return cc;
	}

	/**
	 * Send an HTML email for password recovery.
	 *
	 * @param to      The recipient's email address.
	 * @param subject The subject of the email.
	 * @param password The password to include in the email content.
	 * @throws MessagingException If there's an error while creating the MimeMessage.
	 */
	public void forgotMail(String to, String subject, String password) throws MessagingException {
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setFrom("yayne16@gmail.com");
		helper.setTo(to);
		helper.setSubject(subject);
		String htmlMsg = "<p><b>Your Login details for Abay Store</b><br><b>Email: </b> " + to + " <br><b>Password: </b> " + password + "<br><a href=\"http://localhost:4200/\">Click here to login</a></p>";
		message.setContent(htmlMsg, "text/html");

		emailSender.send(message);


	}
}
