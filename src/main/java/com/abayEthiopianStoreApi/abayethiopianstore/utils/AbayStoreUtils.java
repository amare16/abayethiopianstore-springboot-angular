package com.abayEthiopianStoreApi.abayethiopianstore.utils;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class AbayStoreUtils {

	// Private constructor to prevent instantiation of the utility class
	private AbayStoreUtils() {}

	/**
	 * Generate a ResponseEntity with the given response message and HTTP status.
	 *
	 * @param responseMessage The message to be included in the response body.
	 * @param httpStatus      The HTTP status code to be included in the response.
	 * @return A ResponseEntity with the provided response message and HTTP status.
	 */
	public static ResponseEntity<String> getResponseEntity(String responseMessage, HttpStatus httpStatus) {
		return new ResponseEntity<String>("{\"message\":\""+ responseMessage +"\"}", httpStatus);
	}

	/**
	 * Generate a UUID with the current timestamp.
	 *
	 * @return A UUID string.
	 */
	public static String getUUID() {
		Date date = new Date();
		long time = date.getTime();
		return "BILL-" + time;
	}

	/**
	 * Convert a JSON string to a JSONArray.
	 *
	 * @param data The JSON string to be converted.
	 * @return A JSONArray created from the provided JSON string.
	 * @throws JSONException If there's an error while creating the JSONArray.
	 */
	public static JSONArray getJsonArrayFromString(String data) throws JSONException {
		JSONArray jsonArray = new JSONArray(data);
		return jsonArray;
	}

	/**
	 * Convert a JSON string to a Map.
	 *
	 * @param data The JSON string to be converted.
	 * @return A Map created from the provided JSON string.
	 */
	public static Map<String, Object> getMapFromJson(String data) {
		if (!Strings.isNullOrEmpty(data))
			return new Gson().fromJson(data, new TypeToken<Map<String, Object>>(){}.getType());
		return new HashMap<>();
	}

	/**
	 * Check if a file exists at the specified path.
	 *
	 * @param path The path of the file to be checked.
	 * @return True if the file exists, otherwise false.
	 */
	public static Boolean isFileExist(String path) {
		log.info("Inside isFileExist {}", path);

		try {
			File file = new File(path);
			return (file != null && file.exists() ? Boolean.TRUE : Boolean.FALSE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
