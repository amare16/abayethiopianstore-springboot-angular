package com.abayEthiopianStoreApi.abayethiopianstore.restImpl;

import com.abayEthiopianStoreApi.abayethiopianstore.constants.AbayStoreConstants;
import com.abayEthiopianStoreApi.abayethiopianstore.entity.Bill;
import com.abayEthiopianStoreApi.abayethiopianstore.rest.BillRest;
import com.abayEthiopianStoreApi.abayethiopianstore.service.BillService;
import com.abayEthiopianStoreApi.abayethiopianstore.utils.AbayStoreUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Implementation of the BillRest interface for managing bill-related REST API endpoints.
 */
@RestController
public class BillRestImpl implements BillRest {

    @Autowired
    BillService billService;

    /**
     * Generate a bill report based on the provided request data.
     * @param requestMap The map containing request data.
     * @return A response entity indicating the success or failure of the operation.
     */
    @Override
    public ResponseEntity<String> generateBillReport(Map<String, Object> requestMap) {
        try {
            return billService.generateBillReport(requestMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Get a list of all bills.
     * @return A response entity containing a list of Bill objects.
     */
    @Override
    public ResponseEntity<List<Bill>> getBills() {
        try {
            return billService.getBills();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Generate a PDF document based on the provided request data.
     * @param requestMap The map containing request data.
     * @return A response entity containing the generated PDF content as bytes.
     */
    @Override
    public ResponseEntity<byte[]> getPdf(Map<String, Object> requestMap) {
        try {
            return billService.getPdf(requestMap);
        } catch (Exception e) {
        e.printStackTrace();
    }
        return null;
    }

    /**
     * Delete a bill and its associated PDF document from the local PC based on the provided ID.
     * @param id The ID of the bill to be deleted.
     * @return A response entity indicating the success or failure of the operation.
     */
    @Override
    public ResponseEntity<String> deleteBillAndPdfFromLocalPc(Integer id) {
        try {
            return billService.deleteBillAndPdfFromLocalPc(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
