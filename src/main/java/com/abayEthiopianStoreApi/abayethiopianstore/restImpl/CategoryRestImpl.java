package com.abayEthiopianStoreApi.abayethiopianstore.restImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.abayEthiopianStoreApi.abayethiopianstore.constants.AbayStoreConstants;
import com.abayEthiopianStoreApi.abayethiopianstore.entity.Category;
import com.abayEthiopianStoreApi.abayethiopianstore.rest.CategoryRest;
import com.abayEthiopianStoreApi.abayethiopianstore.service.CategoryService;
import com.abayEthiopianStoreApi.abayethiopianstore.utils.AbayStoreUtils;
import com.abayEthiopianStoreApi.abayethiopianstore.wrapper.UserWrapper;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

/**
 * Implementation of the CategoryRest interface for managing category-related REST API endpoints.
 */
@RestController
public class CategoryRestImpl implements CategoryRest {

	@Autowired
	CategoryService categoryService;

	/**
	 * Add a new category using the provided request data.
	 * @param requestMap The map containing request data, including category details.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> addNewCategory(Map<String, String> requestMap) {
		try {
			return categoryService.addNewCategory(requestMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Get a list of all categories based on the optional filter value.
	 * @param filterValue The optional filter value to narrow down the results.
	 * @return A response entity containing a list of Category objects.
	 */
	@Override
	public ResponseEntity<List<Category>> getAllCategories(String filterValue) {
		try {
			return categoryService.getAllCategories(filterValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Update a category using the provided request data.
	 * @param requestMap The map containing request data, including updated category details.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> updateCategory(Map<String, String> requestMap) {
		try {
			return categoryService.updateCategory(requestMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
