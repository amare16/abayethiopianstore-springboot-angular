package com.abayEthiopianStoreApi.abayethiopianstore.restImpl;

import com.abayEthiopianStoreApi.abayethiopianstore.constants.AbayStoreConstants;
import com.abayEthiopianStoreApi.abayethiopianstore.rest.ProductRest;
import com.abayEthiopianStoreApi.abayethiopianstore.service.ProductService;
import com.abayEthiopianStoreApi.abayethiopianstore.utils.AbayStoreUtils;
import com.abayEthiopianStoreApi.abayethiopianstore.wrapper.ProductWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the ProductRest interface for managing product-related REST API endpoints.
 */
@RestController
public class ProductRestImpl implements ProductRest {

    @Autowired
    ProductService productService;

	/**
	 * Add a new product using the provided request data.
	 * @param requestMap The map containing request data, including product details.
	 * @return A response entity indicating the success or failure of the operation.
	 */
    @Override
    public ResponseEntity<String> addNewProduct(Map<String, String> requestMap) {
        try {
            return productService.addNewProduct(requestMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
    }

	/**
	 * Get a list of all products.
	 * @return A response entity containing a list of ProductWrapper objects.
	 */
	@Override
	public ResponseEntity<List<ProductWrapper>> getAllProducts() {
		try {
			return productService.getAllProducts();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Update a product using the provided request data.
	 * @param requestMap The map containing request data, including updated product details.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> updateProduct(Map<String, String> requestMap) {
		try {
			return productService.updateProduct(requestMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Delete a product based on the provided product ID.
	 * @param productId The ID of the product to be deleted.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> deleteProduct(Integer productId) {
		try {
			return productService.deleteProduct(productId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Update the status of a product using the provided request data.
	 * @param requestMap The map containing request data, including product status details.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> updateProductStatus(Map<String, String> requestMap) {
		try {
			return productService.updateProductStatus(requestMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Get a list of products based on the provided category ID.
	 * @param id The ID of the category to filter products by.
	 * @return A response entity containing a list of ProductWrapper objects.
	 */
	@Override
	public ResponseEntity<List<ProductWrapper>> getProductByCategory(Integer id) {
		try {
			return productService.getProductByCategory(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Get product details based on the provided product ID.
	 * @param id The ID of the product to retrieve details for.
	 * @return A response entity containing a ProductWrapper object.
	 */
	@Override
	public ResponseEntity<ProductWrapper> getProductById(Integer id) {
		try {
			return productService.getProductById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ProductWrapper(), HttpStatus.INTERNAL_SERVER_ERROR);
	}


}
