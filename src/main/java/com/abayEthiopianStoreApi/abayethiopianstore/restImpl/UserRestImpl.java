package com.abayEthiopianStoreApi.abayethiopianstore.restImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.abayEthiopianStoreApi.abayethiopianstore.constants.AbayStoreConstants;
import com.abayEthiopianStoreApi.abayethiopianstore.rest.UserRest;
import com.abayEthiopianStoreApi.abayethiopianstore.service.UserService;
import com.abayEthiopianStoreApi.abayethiopianstore.utils.AbayStoreUtils;
import com.abayEthiopianStoreApi.abayethiopianstore.wrapper.UserWrapper;

/**
 * Implementation of the UserRest interface for managing user-related REST API endpoints.
 */
@RestController
public class UserRestImpl implements UserRest {

	@Autowired
	UserService userService;

	/**
	 * Sign up a new user using the provided request data.
	 * @param requestMap The map containing request data, including user details.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> signUp(Map<String, String> requestMap) {
		try {
			return userService.signUp(requestMap);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Perform user login using the provided request data.
	 * @param requestMap The map containing request data, including user credentials.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> login(Map<String, String> requestMap) {
		try {
			return userService.login(requestMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Get a list of all users.
	 * @return A response entity containing a list of UserWrapper objects.
	 */
	@Override
	public ResponseEntity<List<UserWrapper>> getAllUsers() {
		try {
			System.out.println("get all users: " + userService.getAllUsers());
			return userService.getAllUsers();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<List<UserWrapper>>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Update a user using the provided request data.
	 * @param requestMap The map containing request data, including updated user details.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> updateUser(Map<String, String> requestMap) {
		try {
			return userService.updateUser(requestMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Check the validity of a token for user authentication.
	 * @return A response entity indicating the validity of the token.
	 */
	@Override
	public ResponseEntity<String> checkToken() {
		try {
			return userService.checkToken();
			//System.out.println("check token rest impl: " + userService.checkToken());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Change the password for a user using the provided request data.
	 * @param requestMap The map containing request data, including new password.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> changePassword(Map<String, String> requestMap) {
		try {
			return userService.changePassword(requestMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Initiate the password reset process for a user using the provided request data.
	 * @param requestMap The map containing request data, including user email.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> forgotPassword(Map<String, String> requestMap) {
		try {
			return userService.forgotPassword(requestMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Delete a user based on the provided user ID.
	 * @param userId The ID of the user to be deleted.
	 * @return A response entity indicating the success or failure of the operation.
	 */
	@Override
	public ResponseEntity<String> deleteUser(Integer userId) {
		try {
			return userService.deleteUser(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AbayStoreUtils.getResponseEntity(AbayStoreConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
