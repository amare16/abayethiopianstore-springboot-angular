package com.abayEthiopianStoreApi.abayethiopianstore.restImpl;

import com.abayEthiopianStoreApi.abayethiopianstore.rest.DashboardRest;
import com.abayEthiopianStoreApi.abayethiopianstore.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Implementation of the DashboardRest interface for managing dashboard-related REST API endpoints.
 */
@RestController
public class DashboardRestImpl implements DashboardRest {

    @Autowired
    DashboardService dashboardService;

    /**
     * Get a map containing counts and details for the dashboard.
     * @return A response entity containing the dashboard information.
     */
    @Override
    public ResponseEntity<Map<String, Object>> getCount() {
        return dashboardService.getCount();
    }
}
