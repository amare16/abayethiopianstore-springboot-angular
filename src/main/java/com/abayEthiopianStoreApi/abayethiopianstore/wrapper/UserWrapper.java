package com.abayEthiopianStoreApi.abayethiopianstore.wrapper;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserWrapper {

	// each are similar with @NamedQuery in User.java
	/* UserWrapper user = new UserWrapper(1, "Nina", "Victo", "+339449449", "nina@victo.com", "false"); */
	/* UserWrapper user = new com.abayEthiopianStoreApi.abayethiopianstore.wrapper.UserWrapper(1, "Nina", "Victo", "+339449449", "nina@victo.com", "false"); */


	// Fields to represent user information
	private Integer id;

	private String firstName;

	private String lastName;
	
	private String contactNumber;
	
	private String email;
	
	private String status;

	// Constructor with all fields for creating a user wrapper
	public UserWrapper(Integer id, String firstName, String lastName, String contactNumber, String email,
			String status) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.contactNumber = contactNumber;
		this.email = email;
		this.status = status;
	}

	/*
	 * This class is a wrapper used to encapsulate user-related information. It
	 * contains fields representing different attributes of a user, such as ID, name,
	 * contact information, email, and status. The Lombok annotations are used to
	 * automatically generate standard getter, setter, equals, hashCode, and toString
	 * methods for the fields. The `@NoArgsConstructor` annotation generates a default
	 * no-argument constructor.
	 */

	/*
		In Java, a constructor is a special type of method that is automatically invoked when an object of a class is created.
		Constructors are used to initialize the state of an object and perform any necessary setup or initialization tasks.
		They have the same name as the class they belong to and don't have a return type, not even void.
	 */

	
}
