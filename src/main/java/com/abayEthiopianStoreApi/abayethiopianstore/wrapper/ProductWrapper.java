package com.abayEthiopianStoreApi.abayethiopianstore.wrapper;

import lombok.Data;

@Data
public class ProductWrapper {

	// Fields related to product information
	Integer id;
	
	String name;
	
	String description;
	
	Integer price;
	
	String status;

	// Fields related to product category
	Integer categoryId;
	
	String categoryName;

	// Constructor for creating an empty instance
	public ProductWrapper() {}

	// Constructor with all fields for complete product information
	public ProductWrapper(Integer id, String name, String description, Integer price, String status, Integer categoryId, String categoryName) {
	
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.status = status;
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		
	}

	// Constructor with only essential fields
	public ProductWrapper(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	// Constructor with fields for basic product information
	public ProductWrapper(Integer id, String name, String description, Integer price, String categoryName) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.categoryName = categoryName;
	}

	// Field for storing error messages
	private String errorMessage;

	// Constructor for creating a wrapper with an error message
	public ProductWrapper(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
