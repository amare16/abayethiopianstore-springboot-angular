package com.abayEthiopianStoreApi.abayethiopianstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//import org.springframework.web.bind.annotation.CrossOrigin;


@SpringBootApplication 
@EnableConfigurationProperties
@EntityScan(basePackages = {"com.abayEthiopianStoreApi.abayethiopianstore.entity"})
//@CrossOrigin(origins = "http://localhost:4200") // Replace with your Angular frontend URL
public class AbayEthiopianStoreApplication {

	public static void main(String[] args) {
		// Entry point of the Spring Boot application
		SpringApplication.run(AbayEthiopianStoreApplication.class, args);
	}

	// Configures Cross-Origin Resource Sharing (CORS) for the application
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**") // Maps all endpoints
						.allowedOrigins("http://localhost:4200") // Specifies allowed frontend origin URLs
						.allowedMethods("GET", "POST", "PUT", "DELETE") // Specifies allowed HTTP methods
						.allowedHeaders("*"); // Specifies allowed request headers
			}
		};
	}
}
