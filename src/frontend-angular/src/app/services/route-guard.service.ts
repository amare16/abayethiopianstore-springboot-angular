import { Injectable } from '@angular/core';
import {AuthService} from "./auth.service";
import {ActivatedRouteSnapshot, Router} from "@angular/router";
import {SnackbarService} from "./snackbar.service";
import jwt_decode from "jwt-decode";
import {GlobalConstants} from "../shared/global-constants";

@Injectable({
  providedIn: 'root'
})
export class RouteGuardService {

  constructor(private auth:AuthService,
              private router:Router,
              private snackbarService:SnackbarService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    let expectedRoleArray = route.data;
    console.log("expected role array: ", expectedRoleArray);
    expectedRoleArray = expectedRoleArray['expectedRole'];

    const token:any = localStorage.getItem('token');

    var tokenPayload:any;
    try {
      tokenPayload = jwt_decode(token);
    } catch (err) {
      localStorage.clear();
      this.router.navigate(['/']);
    }

    console.log("Token Payload Role:", tokenPayload.role);
    console.log("Expected Role Array:", expectedRoleArray);

    let expectedRole = '';

    for (let i = 0; i < expectedRoleArray['length']; i++) {
      if (expectedRoleArray[i] == tokenPayload.role) {
        expectedRole = tokenPayload.role;
        break; // Break the loop when a matching role is found
      }
    }
    // console.log("Token Payload Role:", tokenPayload.role);
    // console.log("Expected Role:", expectedRole);

    if (tokenPayload.role == 'ROLE_USER' || tokenPayload.role == 'ROLE_ADMIN') {
      console.log("test auth and token payload: ", this.auth.isAuthenticated() && tokenPayload.role == expectedRole);
      if (this.auth.isAuthenticated() && tokenPayload.role == expectedRole) {
        return true;
      }
      this.snackbarService.openSnackBar(GlobalConstants.unauthorized, GlobalConstants.error);
     // this.router.navigate(['/abaystore/dashboard']);
      return false;
    } else {
      this.router.navigate(['/']);
      localStorage.clear();
      return false;
    }
  }



}
