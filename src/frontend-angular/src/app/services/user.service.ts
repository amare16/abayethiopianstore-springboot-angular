import { Injectable } from '@angular/core';
import {environment} from "../../../environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = environment.apiUrl;
  constructor(private httpClient:HttpClient) { }

  signUp(data:any) {
    // const headers = new HttpHeaders().set('Content-Type', 'application/json');
    //
    // // Set the mode to "no-cors" in the request options
    // const options = {
    //   headers,
    //   mode: 'no-cors'
    // };
    //return this.httpClient.post(`${this.url}/user/signup`, data, options);
    return this.httpClient.post(this.url+"/user/signup", data, {
      headers:new HttpHeaders().set('Content-Type', 'application/json')
    });


  }

  login(data:any) {
    return this.httpClient.post(this.url+"/user/login", data, {
      headers:new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  forgotPassword(data:any) {
    return this.httpClient.post(this.url+"/user/forgotPassword", data, {
      headers:new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  checkToken() {
    // check if the token is valid or not and if it is expired, remove from localstorage and store the new one
    return this.httpClient.get(this.url+"/user/checkToken");
  }

  changePassword(data:any) {
    console.log("data in changePassword: ", data);
    return this.httpClient.post(this.url+"/user/changePassword", data, {
      headers:new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  getUsers() {
    return this.httpClient.get(this.url+"/user/getusers");
  }

  updateUser(data: any) {
    return this.httpClient.post(this.url+"/user/updatestatus", data, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  deleteUser(userId: any) {
    return this.httpClient.delete(`${this.url}/user/deleteUser/${userId}`, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }


}
