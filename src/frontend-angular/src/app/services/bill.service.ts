import { Injectable } from '@angular/core';
import {environment} from "../../../environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BillService {

  url: string = environment.apiUrl;
  constructor(private http: HttpClient) { }

  generateBillReport(data: any) {
    return this.http.post(
      `${this.url}/bill/generateBillReport`,
      data, {
        headers: new HttpHeaders().set('Content-Type', 'application/json')
      }

    );
  }

  getPdf(data: any): Observable<Blob> {
    // @ts-ignore
    return this.http.post(`${this.url}/bill/getPdf`, data, {
      responseType: 'blob' as 'json', // Response type is set to 'blob'
      headers: new HttpHeaders().set('Accept', 'application/pdf')
    });
  }

  getBills() {
    return this.http.get(`${this.url}/bill/getBills`);
  }

  deleteBillAndPdfFromLocalPc(id: any) {
    return this.http.delete(`${this.url}/bill/deleteBillAndPdfFromLocalPc/${id}`, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }
}
