import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environment";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  url = environment.apiUrl;

  constructor(private httpClient:HttpClient) { }

  getDetails() {
    //console.log("getDetails url: ", this.url+"/dashboard/details");
    return this.httpClient.get(this.url+"/dashboard/details");
  }
}
