import { Injectable } from '@angular/core';
import {environment} from "../../../environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  url = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }

  addCategory(data: any) {
    return this.httpClient.post(this.url + "/category/addCategory", data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  updateCategory(data: any) {
    return this.httpClient.post(this.url + "/category/updateCategory", data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  getAllCategories() {
    return this.httpClient.get(this.url + "/category/getAllCategories");
  }
  getFilteredCategories() {
    return this.httpClient.get(this.url + "/category/getAllCategories?filterValue=true");
  }
}
