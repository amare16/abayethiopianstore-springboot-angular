import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environment";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }

  addProduct(data: any) {
    return this.httpClient.post(this.url + "/product/addNewProduct", data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  updateProduct(data: any) {
    return this.httpClient.post(this.url + "/product/updateProduct", data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  getAllProducts() {
    return this.httpClient.get(this.url + "/product/getAllProducts");
  }

  updateProductStatus(data: any) {
    return this.httpClient.post(this.url + "/product/updateProductStatus", data, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  deleteProduct(productId: any) {
    return this.httpClient.delete(`${this.url}/product/deleteProduct/${productId}`, {
      headers: new HttpHeaders().set('Content-Type', "application/json")
    });
  }

  getProductByCategory(id: any) {
    return this.httpClient.get(`${this.url}/product/getProductByCategory/${id}`);
  }

  getProductById(id: any) {
    return this.httpClient.get(`${this.url}/product/getProductById/${id}`);
  }


}
