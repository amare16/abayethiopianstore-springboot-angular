import {AfterViewInit, Component} from '@angular/core';
import {NgxUiLoaderService} from "ngx-ui-loader";
import {SnackbarService} from "../services/snackbar.service";
import {DashboardService} from "../services/dashboard.service";
import {GlobalConstants} from "../shared/global-constants";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements AfterViewInit {

  // To store response messages
  responseMessage: any;
  // To store dashboard data
  data: any;

  // This method is called after the view has been initialized
  ngAfterViewInit() {}

  constructor(private dashboardService: DashboardService, // Injecting DashboardService
              private ngxService: NgxUiLoaderService, // Injecting NgxUiLoaderService
              private snackbarService: SnackbarService, // Injecting SnackbarService
              ) {
    this.ngxService.start(); // Start the loading spinner
    this.dashboardData(); // Fetch dashboard data
  }

  // Fetch dashboard data from the service
  dashboardData() {
    this.dashboardService.getDetails().subscribe((response:any) => {
      this.ngxService.stop(); // Stop the loading spinner
      this.data = response; // Assign the received data to the 'data' property
      console.log("this data: ", this.data);

    }, (error: any) => {
      this.ngxService.stop();
      console.log(error);

      // Handle error messages
      if (error.error?.message) {
        this.responseMessage = error.error?.message;
      } else {
        this.responseMessage = GlobalConstants.genericError;
      }
      // Display error message using the SnackbarService
      this.snackbarService.openSnackBar(this.responseMessage, GlobalConstants.error);
    })
  }
}
