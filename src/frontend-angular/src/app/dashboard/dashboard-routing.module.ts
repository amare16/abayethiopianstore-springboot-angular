// Import necessary Angular modules and components
import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';

// Define the routes for the dashboard module
export const DashboardRoutingModule: Routes = [{
  path: '', // Empty path, which means this component will be loaded when the URL matches the parent route
  component: DashboardComponent // Specifies the component to be loaded when the URL matches the path
}];

