// Import necessary Angular modules and components
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DashboardComponent } from './dashboard.component'; // Importing the DashboardComponent
import {DashboardRoutingModule} from "./dashboard-routing.module"; // Importing the routing module
import {MatCardModule} from "@angular/material/card"; // Importing the Material Card module

@NgModule({
  imports: [
    CommonModule, // Provides common directives like ngFor and ngIf
    FlexLayoutModule, // Provides tools for responsive web design with Flexbox
    RouterModule.forChild(DashboardRoutingModule), // Configured routes for the dashboard
    MatCardModule // Provides Material Design cards
  ],
  declarations: [DashboardComponent] // Declaring the DashboardComponent in this module
})
export class DashboardModule { }
