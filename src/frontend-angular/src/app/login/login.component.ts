import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UserService} from "../services/user.service";
import {SnackbarService} from "../services/snackbar.service";
import {MatDialogRef} from "@angular/material/dialog";
import {NgxUiLoaderService} from "ngx-ui-loader";
import {ForgotPasswordComponent} from "../forgot-password/forgot-password.component";
import {GlobalConstants} from "../shared/global-constants";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide = true;
  loginForm: any = FormGroup;
  responseMessage:any;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private userService: UserService,
              private snackbarService: SnackbarService,
              public dialogRef:MatDialogRef<ForgotPasswordComponent>,
              private ngxService: NgxUiLoaderService) {}

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern(GlobalConstants.emailRegex)]],
      password: [null, [Validators.required]]
    })
  }

  handleLoginSubmit() {
    this.ngxService.start();
    var formData = this.loginForm.value;
    var data = {
      email: formData.email,
      password: formData.password
    }
    console.log("loin form data: " + formData.email + " " + formData.password);
    this.userService.login(data).subscribe((response:any) => {
      this.ngxService.stop();
      this.responseMessage = response?.message;
      this.dialogRef.close();
      localStorage.setItem('token', response.token);
      console.log("Stored token in local storage: " + response.token);
      //this.router.navigate(['/abaystore/dashboard']);
      this.redirectToDashboard();
    }, (error) => {
      if (error.error?.message) {
        this.ngxService.stop();
        this.responseMessage = error.error?.message;
        console.log("responseMessage: ", this.responseMessage);
      } else {
        this.responseMessage = GlobalConstants.genericError;
      }
      this.snackbarService.openSnackBar(this.responseMessage, GlobalConstants.error);
    })
  }
  redirectToDashboard(): void {
    this.router.navigate(['/abaystore/dashboard']);
  }
}
