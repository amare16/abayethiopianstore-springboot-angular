import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {SignUpComponent} from "../sign-up/sign-up.component";
import {ForgotPasswordComponent} from "../forgot-password/forgot-password.component";
import {LoginComponent} from "../login/login.component";
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";
import {catchError, EMPTY} from "rxjs";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private dialog:MatDialog,
              private userService:UserService, private router:Router) {
  }
  ngOnInit() : void {
    this.userService.checkToken().subscribe((response:any) => {
      this.router.navigate(['/abaystore/dashboard']);
      console.log("checkToken - response: ", response);
    }, (error:any) => {
      console.error("ngoninit check token error: " + error);
    });
  }

  handleSignupAction() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "550px";
    console.log("dialog: ", dialogConfig)
    this.dialog.open(SignUpComponent, dialogConfig);
  }

  forgotPasswordAction() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "550px";
    console.log("dialog: ", dialogConfig)
    this.dialog.open(ForgotPasswordComponent, dialogConfig);
  }

  handleLoginAction() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "550px";
    console.log("dialog: ", dialogConfig)
    this.dialog.open(LoginComponent, dialogConfig);
  }
}
