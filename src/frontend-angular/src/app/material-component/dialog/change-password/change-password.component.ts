import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../services/user.service";
import {MatDialogRef} from "@angular/material/dialog";
import {NgxUiLoaderService} from "ngx-ui-loader";
import {SnackbarService} from "../../../services/snackbar.service";
import {GlobalConstants} from "../../../shared/global-constants";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  oldPassword = true;
  newPassword = true;
  confirmPassword = true;
  changePasswordForm: any = FormGroup;
  responseMessage: any;

  // showNewPassword: boolean = false;
  // showConfirmPassword: boolean = false;



  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              public dialogRef: MatDialogRef<ChangePasswordComponent>,
              private ngxService: NgxUiLoaderService,
              private snackbarService: SnackbarService) {
  }

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      oldPassword: [null, [Validators.required]],
      newPassword: [null, [Validators.required]],
      confirmPassword: [null, [Validators.required]]
    });
  }

  // validateSubmit() {
  //   if (this.changePasswordForm.controls['newPassword'].value !== this.changePasswordForm.controls['confirmPassword']) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
  validateSubmit() {
    const newPassword = this.changePasswordForm.controls['newPassword'].value;
    const confirmPassword = this.changePasswordForm.controls['confirmPassword'].value;
    return newPassword !== confirmPassword;
  }


  handlePasswordChangeSubmit() {
    this.ngxService.start();
    const formData = this.changePasswordForm.value;
    const data = {
      oldPassword: formData.oldPassword,
      newPassword: formData.newPassword,
      confirmPassword: formData.confirmPassword
    };

    this.userService.changePassword(data).subscribe(
      (response: any) => {
        this.ngxService.stop();
        this.dialogRef.close();
        this.responseMessage = response?.message;
        this.snackbarService.openSnackBar(this.responseMessage, '');
      },
      (error) => {
        console.log('Error in handlePasswordChangeSubmit:', error);
        this.ngxService.stop();

        if (error?.error?.message) {
          // Check for "Incorrect Old Password" error
          if (error.error.message === 'Incorrect Old Password') {
            this.changePasswordForm.get('oldPassword').setErrors({ incorrectPassword: true });
          }
          this.responseMessage = error.error.message;
        } else {
          this.responseMessage = 'An error occurred while changing the password.';
        }

        this.snackbarService.openSnackBar(this.responseMessage, GlobalConstants.error);
      }
    );
  }

}
