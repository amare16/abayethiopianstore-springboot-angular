import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NgxUiLoaderConfig, NgxUiLoaderModule, SPINNER} from "ngx-ui-loader";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FlexLayoutModule, FlexModule} from "@angular/flex-layout";
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import {TokenInterceptor} from "./services/token.interceptor";
import {MaterialModule} from "./shared/material-module";
import {SidebarComponent} from "./layouts/full/sidebar/sidebar.component";
import {SharedModule} from "./shared/shared.module";
import { FullComponent } from './layouts/full/full.component';
import { HeaderComponent } from './layouts/full/header/header.component';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  text: "Loading...",
  textColor: "#FFFFFF",
  textPosition: "center-center",
  bgsColor: "#0a3cf6",
  fgsColor: "#0a3cf6",
  //fgsType: SPINNER.squareJellyBox,
  fgsType: SPINNER.ballSpinFadeRotating,
  fgsSize: 100,
  hasProgressBar: false
}
@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    HomeComponent,
    ForgotPasswordComponent,
    LoginComponent,
    FullComponent,
    HeaderComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    ReactiveFormsModule,
    MaterialModule,
    FormsModule,
    FlexModule,
    FlexLayoutModule,
    SharedModule
  ],
  providers: [HttpClientModule, { provide: HTTP_INTERCEPTORS, useClass:TokenInterceptor, multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
